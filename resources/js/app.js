require('./bootstrap');
import Vue from 'vue';
import App from './vue/app'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faPlusSquare, faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faPlusSquare, faTrash)

Vue.component('font-awesome-icon', FontAwesomeIcon)

import Toaster from 'v-toaster'
// You need a specific loader for CSS files like https://github.com/webpack/css-loader

import 'v-toaster/dist/v-toaster.css'
// optional set default imeout, the default is 10000 (10 seconds).

Vue.use(Toaster, {timeout: 5000})

let app= new Vue({
    el:'#app',
    components:{
        App
    }
})
